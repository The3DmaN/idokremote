import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {

    id: pageremote

    Item {
        anchors.centerIn: parent
        width: textsendDialog.width
        height: textsendDialog.height

        Dialog {
            id: textsendDialog
            width: 300
            height: 210
            modal: true
            title: "Send Text"

            Column {
                width: parent.width
                TextField {
                    id: sendtext
                    width: parent.width - 10
                    placeholderText: qsTr("Enter text to send")
                }
                Row {
                    CheckBox {
                        id: sendcomp
                    }
                    Label {
                        topPadding: 15
                        text: "Finish after send"
                    }
                }
                Row {
                    spacing: 5
                    width: parent.width
                    layoutDirection: Qt.RightToLeft
                    Button {
                        text: "Cancel"
                        onClicked: {
                            sendtext.text = ""
                            sendcomp.checkState = 0
                            textsendDialog.close();
                        }
                    }
                    Button {
                        text: "OK"
                        onClicked: {
                            if (sendcomp.checked)
                            {
                                request(
                                            "POST",
                                            "\"Input.SendText\",\"params\":{\"text\":\"" + sendtext.text + "\",\"done\":true}",
                                            function (o) { processResults(o); }
                                            );
                                sendtext.text = ""
                                sendcomp.checkState = 0
                                textsendDialog.close();
                            } else {
                                request(
                                            "POST",
                                            "\"Input.SendText\",\"params\":{\"text\":\"" + sendtext.text + "\",\"done\":false}",
                                            function (o) { processResults(o); }
                                            );
                                sendtext.text = ""
                                sendcomp.checkState = 0
                                textsendDialog.close();
                            }
                        }
                    }
                }
            }
        }
    }

    Item {
        anchors.centerIn: parent
        width: tubesendDialog.width
        height: tubesendDialog.height

        Dialog {
            id: tubesendDialog
            width: 300
            height: 210
            modal: true
            title: "Send YT Video"

            Column {
                width: parent.width
                TextField {
                    id: sendtube
                    width: parent.width - 10
                    placeholderText: qsTr("Enter YT Video ID")
                }
                Row {
                    spacing: 5
                    width: parent.width
                    layoutDirection: Qt.RightToLeft
                    Button {
                        text: "Cancel"
                        onClicked: {
                            sendtube.text = ""
                            tubesendDialog.close();
                        }
                    }
                    Button {
                        text: "OK"
                        onClicked: {
                            request(
                                        "POST",
                                        "\"Player.Open\",\"params\":{\"item\":{\"file\":\"plugin://plugin.video.youtube/?path=/root/video&action=play_video&videoid=" + sendtube.text + "\"}}",
                                        function (o) { processResults(o); }
                                        );
                            sendtube.text = ""
                            tubesendDialog.close();
                            stackView.pop()
                            stackView.push("NowPlaying.qml")
                        }
                    }
                }
            }
        }
    }


    Item {
        anchors.centerIn: parent
        width: subsDialog.width
        height: subsDialog.height

        Dialog {
            id: subsDialog
            width: 300
            height: 120
            modal: true
            title: "Subtitles"

            ComboBox {
                id: subsCombo
                textRole: "lang"
                width: parent.width
                model: ListModel { id: subsModel }
                onActivated: {
                    var params =
                            "{\"playerid\":1,\"subtitle\":".concat(
                                subsModel.get(currentIndex).index, ", \"enable\":true}"
                                );
                    if (subsModel.get(currentIndex).index === "-1")
                    {
                        params = "{\"playerid\":1,\"subtitle\":\"off\"}";

                    }
                    request(
                                "POST",
                                "\"Player.SetSubtitle\",\"params\":".concat(params),
                                function (o) { processResults(o); }
                                );
                    subsDialog.close();
                }
            }
        }
    }

    Item {
        anchors.centerIn: parent
        width: audDialog.width
        height: audDialog.height

        Dialog {
            id: audDialog
            width: 300
            height: 120
            modal: true
            title: "Audio"

            ComboBox {
                id: audCombo
                textRole: "lang"
                width: parent.width
                model: ListModel { id: audModel }
                onActivated: {
                    var params =
                            "{\"playerid\":1,\"stream\":".concat(
                                audModel.get(currentIndex).index, "}"
                                );
                    request(
                                "POST",
                                "\"Player.SetAudioStream\",\"params\":".concat(params),
                                function (o) { processResults(o); }
                                );
                    audDialog.close();
                }
            }
        }
    }

    ColumnLayout {
        width: pageremote.width
        Row {
            topPadding: 10
            Layout.alignment: Qt.AlignHCenter
            Label {
                width: remoteselect.width
                horizontalAlignment: Text.AlignHCenter
                text: "Location Selection"
                color: "white"
            }
        }
        Row{
            Layout.alignment: Qt.AlignHCenter
            ComboBox {
                id: remoteselect
                width: 250
                textRole: "name"
                model: userModel
                onActivated: {
                    settings.name = userModel.get(currentIndex).name;
                    settings.ipaddr = userModel.get(currentIndex).ip;
                    settings.portnum = userModel.get(currentIndex).iport;
                    settings.user = userModel.get(currentIndex).uname;
                    settings.password = userModel.get(currentIndex).pword;
                    settings.index = currentIndex;
                }
                Component.onCompleted: {
                    currentIndex = settings.index
                }
            }
        }

        Row {
            topPadding: 1
            Layout.alignment: Qt.AlignHCenter
            spacing: 5

            Image {
                id: buttonskipleft
                width: 50
                height: 50
                source: "images/skipleft.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.GoTo\",\"params\":{\"playerid\":1,\"to\":\"previous\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonskipleft.scale = 1.10
                    }
                    onReleased: {
                        buttonskipleft.scale = 1
                    }
                }
            }

            Image {
                id: buttonffleft
                width: 50
                height: 50

                source: "images/ffleft.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.Seek\",\"params\":{\"playerid\":1,\"value\":\"smallbackward\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonffleft.scale = 1.10
                    }
                    onReleased: {
                        buttonffleft.scale = 1
                    }
                }
            }

            Image {
                id: buttonplaypause
                width: 50
                height: 50

                source: "images/playpause.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.PlayPause\",\"params\":{\"playerid\":1}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonplaypause.scale = 1.10
                    }
                    onReleased: {
                        buttonplaypause.scale = 1
                    }
                }
            }

            Image {
                id: buttonstop
                width: 50
                height: 50

                source: "images/stop.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.Stop\",\"params\":{\"playerid\":1}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonstop.scale = 1.10
                    }
                    onReleased: {
                        buttonstop.scale = 1
                    }
                }
            }

            Image {
                id: buttonffright
                width: 50
                height: 50

                source: "images/ffright.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.Seek\",\"params\":{\"playerid\":1,\"value\":\"smallforward\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonffright.scale = 1.10
                    }
                    onReleased: {
                        buttonffright.scale = 1
                    }
                }
            }

            Image {
                id: buttonskipright
                width: 50
                height: 50

                source: "images/skipright.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Player.GoTo\",\"params\":{\"playerid\":1,\"to\":\"next\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonskipright.scale = 1.10
                    }
                    onReleased: {
                        buttonskipright.scale = 1
                    }
                }
            }
        }

        Row {
            topPadding: 10
            spacing: 10
            Layout.alignment: Qt.AlignHCenter

            Image {
                id: buttonhome
                y: 35
                width: 75
                height: 75
                source: "images/home.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Home\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonhome.scale = 1.10
                    }
                    onReleased: {
                        buttonhome.scale = 1
                    }
                }
            }

            Image {
                id: buttontop
                width: 110
                height: 110
                source: "images/topbutton.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Up\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttontop.scale = 1.10
                    }
                    onReleased: {
                        buttontop.scale = 1
                    }
                }
            }

            Image {
                id: buttoninfo
                width: 75
                height: 75
                y: 35
                source: "images/info.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.info\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttoninfo.scale = 1.10
                    }
                    onReleased: {
                        buttoninfo.scale = 1
                    }
                }
            }
        }

        Row {
            topPadding: 5
            spacing: 10
            Layout.alignment: Qt.AlignHCenter

            Image {
                id: buttonleft
                width: 110
                height: 110
                source: "images/leftbutton.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Left\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonleft.scale = 1.10
                    }
                    onReleased: {
                        buttonleft.scale = 1
                    }
                }
            }

            Image {
                id: buttoncenter
                width: 110
                height: 110

                source: "images/centerbutton.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Select\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttoncenter.scale = 1.10
                    }
                    onReleased: {
                        buttoncenter.scale = 1
                    }
                }
            }

            Image {
                id: buttonright
                width: 110
                height: 110
                source: "images/rightbutton.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Right\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonright.scale = 1.10
                    }
                    onReleased: {
                        buttonright.scale = 1
                    }
                }
            }
        }

        Row {
            topPadding: 5
            spacing: 10
            Layout.alignment: Qt.AlignHCenter

            Image {
                id: buttonback
                width: 75
                height: 75
                source: "images/back.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Back\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonback.scale = 1.10
                    }
                    onReleased: {
                        buttonback.scale = 1
                    }
                }
            }

            Image {
                id: buttonbottom
                width: 110
                height: 110
                source: "images/bottombutton.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.Down\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonbottom.scale = 1.10
                    }
                    onReleased: {
                        buttonbottom.scale = 1
                    }
                }
            }

            Image {
                id: buttonmenu
                width: 75
                height: 75
                source: "images/menu.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.ContextMenu\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonmenu.scale = 1.10
                    }
                    onReleased: {
                        buttonmenu.scale = 1
                    }
                }
            }

        }

        Row {
            topPadding: 10
            Layout.alignment: Qt.AlignHCenter
            spacing: 5

            Image {
                id: buttonaudio
                width: 50
                height: 50
                source: "images/audio.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "GET",
                                    "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"currentaudiostream\",\"audiostreams\"]}",
                                    function (o)
                                    {
                                        var resp = processResults(o)["result"];

                                        var audio = resp["audiostreams"];
                                        audModel.clear();

                                        audCombo.currentIndex = 0;
                                        if (audio.length > 0)
                                        {
                                            for(var s in audio)
                                            {
                                                var lang = audio[s]["language"];
                                                audModel.append({
                                                                    "index": audio[s]["index"].toString(),
                                                                    "lang": "[".concat((lang.length > 0 ? lang : "unknown"), "] ", audio[s]["name"])
                                                                });
                                            }


                                            var currentAudio = resp["currentaudiostream"];
                                            if (currentAudio !== null)
                                            {
                                                for (var i = 0; i < audModel.count; i++)
                                                {
                                                    if (audModel.get(i).index === currentAudio["index"].toString())
                                                    {
                                                        audCombo.currentIndex = i;
                                                        break;
                                                    }
                                                }

                                            }
                                        }


                                        audDialog.open();
                                    }
                                    );
                    }
                    onPressed: {
                        buttonaudio.scale = 1.10
                    }
                    onReleased: {
                        buttonaudio.scale = 1
                    }
                }
            }
            Image {
                id: buttonsubs
                width: 50
                height: 50
                source: "images/subs.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "GET",
                                    "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"subtitleenabled\",\"currentsubtitle\",\"subtitles\"]}",
                                    function (o)
                                    {
                                        var resp = processResults(o)["result"];

                                        var subtitles = resp["subtitles"];
                                        subsModel.clear();
                                        subsModel.append({
                                                             "index": "-1",
                                                             "lang": "- no subtitles -"
                                                         });
                                        subsCombo.currentIndex = 0;
                                        if (subtitles.length > 0)
                                        {
                                            for(var s in subtitles)
                                            {
                                                var lang = subtitles[s]["language"];
                                                subsModel.append({
                                                                     "index": subtitles[s]["index"].toString(),
                                                                     "lang": "[".concat((lang.length > 0 ? lang : "unknown"), "] ", subtitles[s]["name"])
                                                                 });
                                            }

                                            if(resp["subtitleenabled"] === true)
                                            {
                                                var currentSubtitle = resp["currentsubtitle"];
                                                if (currentSubtitle !== null)
                                                {
                                                    for (var i = 0; i < subsModel.count; i++)
                                                    {
                                                        if (subsModel.get(i).index === currentSubtitle["index"].toString())
                                                        {
                                                            subsCombo.currentIndex = i;
                                                            break;
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        subsDialog.open();
                                    }
                                    );

                    }
                    onPressed: {
                        buttonsubs.scale = 1.10
                    }
                    onReleased: {
                        buttonsubs.scale = 1
                    }
                }
            }

            Image {
                id: buttonosd
                width: 50
                height: 50
                source: "images/osd.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "POST",
                                    "\"Input.showOSD\"",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonosd.scale = 1.10
                    }
                    onReleased: {
                        buttonosd.scale = 1
                    }
                }
            }
            Image {
                id: buttontext
                width: 50
                height: 50
                source: "images/text.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        textsendDialog.open();
                    }
                    onPressed: {
                        buttontext.scale = 1.10
                    }
                    onReleased: {
                        buttontext.scale = 1
                    }
                }

            }

            Image {
                id: buttontube
                width: 50
                height: 50
                source: "images/ytsend.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        tubesendDialog.open();
                    }
                    onPressed: {
                        buttontube.scale = 1.10
                    }
                    onReleased: {
                        buttontube.scale = 1
                    }

                }

            }

        }
    }
}
