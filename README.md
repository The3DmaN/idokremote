# Idok Remote

Idok Remote is a basic remote for Kodi built specificaly to run on Linux Mobile (It also works on desktop)

This remote features the ability to connect to multiple instances and has a combobox selection to choose which one to control right on the remote screen. Perfect for those that have multiple RPI's with Kodi.  

**Features included:**

- Ability to add and select multiple Kodi instances/devices
- Button controls
- Audio and Subtitle selection
- Text entry
- Send to YouTube
- "Now Playing" page (with seeking slider)
- Movie select page.

# Install:

**Manjaro/Arch:**

- Install from AUR (https://aur.archlinux.org/packages/idokremote-git/)

**Mobian/Debian/Ubuntu:**

- Download aarch64 or x64 deb from [here](https://gitlab.com/The3DmaN/the3dman.gitlab.io/-/tree/master/content/files/deb/idokremote) then in a terminal cd into download directory and install with `sudo apt-get install ./IdokRemote_XX_aarch64` or `sudo apt-get install ./IdokRemote_XX_x64` Note: Replace "XX" with package version downloaded and ensure you are using the correct architecture

# Screenshot

![Screenshot](img/idokremote.jpg "Remote Screenshot")
![Screenshot](img/idokremote-ml.jpg "Movie List Screenshot")
![Screenshot](img/idokremote-mi.jpg "Movie Info Screenshot")
![Screenshot](img/idokremote-np.jpg "Now Playing Screenshot")
