import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt.labs.settings 1.1
import QtQuick.Controls.Material 2.3

ApplicationWindow {
    id: idok
    width: 360
    height: 720
    visible: true
    title: qsTr("Idok Remote")
    flags: {
        if (screen.width < 800) {
            Qt.FramelessWindowHint
        }
    }
    Material.theme: {
        Material.primary="#212121"
        Material.accent="white"
        Material.background="bluegrey"
        Material.foreground="white"
    }

    property string idokURL: "http://".concat(settings.ipaddr.trim(), ":", settings.portnum.trim(), "/jsonrpc");
    property string methodTemplate: "{\"jsonrpc\":\"2.0\",\"method\":-=PLACEHOLDER=-,\"id\":1}"

    property string datastore: ""

    property bool wasTimeout: false

    Settings {
        id: settings

        property alias x: idok.x
        property alias y: idok.y
        //property alias width: idok.width
        //property alias height: idok.height
        property alias datastore: idok.datastore

        property string ipaddr: ""
        property string portnum: ""
        property string user: ""
        property string password: ""
        property string name: ""
        property int index: 0
    }

    Component.onCompleted: {
        if (datastore) {
            userModel.clear()
            var datamodel = JSON.parse(datastore)
            for (var i = 0; i < datamodel.length; ++i) userModel.append(datamodel[i])
        }
    }

    onClosing: {
        var datamodel = []
        for (var i = 0; i < userModel.count; ++i) datamodel.push(userModel.get(i))
        datastore = JSON.stringify(datamodel)
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        Row {
            spacing: 10
            ToolButton {
                id: toolButton
                text: stackView.depth > 1 ? "\u25C0" : "\u2630"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                    } else {
                        drawer.open()
                    }
                }
            }
        }

    }

    Drawer {
        id: drawer
        width: idok.width * 0.66
        height: idok.height

        Column {
            anchors.fill: parent
            ItemDelegate {
                text: qsTr("Now Playing")
                width: parent.width
                onClicked: {
                    stackView.push("NowPlaying.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Movies")
                width: parent.width
                onClicked: {
                    stackView.push("MoviesPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("SetForm.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        //initialItem: "RemoteForm.qml"
        anchors.fill: parent
        // Used Component.onCompleted since using 'initialItem:' causes settings not to load on RemoteForm page. Bug??
        Component.onCompleted: {
            stackView.push("RemoteForm.qml")
        }
    }

    Item {
        anchors.centerIn: parent
        width: errorDiag.width
        height: errorDiag.height

        Dialog {
            id: errorDiag
            width: 300
            height: 160
            modal: true
            title: "Error"
            contentItem: Text {
                id: errortxt
                color: "white"
                text: ""
            }
        }
    }

    function request(method, params, callback)
    {
        if (method !== "GET" && method !== "POST")
        {
            let errorText = "Unknown HTTP method: ".concat(method);
            console.log(errorText);
            errortxt.text = errorText;
            errorDiag.open();
            return;
        }

        params = methodTemplate.replace("-=PLACEHOLDER=-", params);
        if (method === "GET")
        {
            params = "?request=".concat(encodeURIComponent(params));
        }

        let req = new XMLHttpRequest();
        req.onreadystatechange = (function(myreq) {
            return function() {
                switch (req.readyState)
                {
                case 0:
                    break;
                case 4:
                    callback(myreq);
                    break;
                }
            }
        })(req);

        req.open(method, method === "GET" ? idokURL.concat(params) : idokURL);
        req.setRequestHeader("Content-Type", "application/json");
        req.setRequestHeader("Authorization", "Basic ".concat(Qt.btoa("".concat(settings.user, ":", settings.password))));

        let reqTimer = Qt.createQmlObject("import QtQuick 2.12; Timer {interval:3000;}", idok, "REQtimer");
        reqTimer.triggered.connect(function() {
            if (req.readyState !== 4)
            {
                wasTimeout = true;
                req.abort();
            }
        });
        reqTimer.start();

        if (method === "GET") { req.send(); }
        else { req.send(params); }
    }

    function processResults(req)
    {
        if (req.status === 200)
        {
            var jsn = JSON.parse(req.responseText);
            if (!jsn.hasOwnProperty("error")) { return jsn; }
            else
            {
                errortxt.text =
                        "Error has occurred<br/>Code: ".concat(
                            jsn["error"]["code"], "<br/>Error: ", jsn["error"]["message"]
                            );
                //errorDiag.open();
            }
        }
        else
        {
            if (wasTimeout === false)
            {
                errortxt.text =
                        "Error has occurred<br/>Code: ".concat(
                            req.status, "<br/>Status: ", req.statusText
                            );
                //errorDiag.open();
            }
            else
            {
                wasTimeout = false;
                errortxt.text = "Timeout. Check player or settings.";
                errorDiag.open();
            }
        }
    }
    ListModel {
        id: userModel

        ListElement {
            name: "Room"
            ip: "192.168.X.X"
            iport: "8080"
            uname: "Test"
            pword: "1234"
        }
    }
}
