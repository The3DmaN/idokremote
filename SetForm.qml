import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt.labs.settings 1.1
import QtQuick.Layouts 1.3

Page {

    Item {
        anchors.centerIn: parent
        width: verifyDialog.width
        height: verifyDialog.height

        Dialog {
            id: verifyDialog
            width: 300
            height: 200
            modal: true
            clip: true
            title: "Remove Location"

            ColumnLayout {
                height: parent.height

                RowLayout{
                    Text {
                        id: devicetext
                        color: "white"
                        text: qsTr("Are you sure?")
                    }
                }
                RowLayout {
                    Layout.alignment: Qt.AlignBottom | Qt.AlignRight
                    spacing: 10
                    Button {
                        id: cancelbutton
                        text: "Cancel"
                        onClicked: {
                            verifyDialog.close()
                        }
                    }
                    Button {
                        id: acceptbutton
                        text: "OK"
                        onClicked: {
                            userModel.remove(list.currentIndex)
                            verifyDialog.close()
                        }
                    }
                }

            }

        }
    }

    Column {
        id: column
        width: parent.width
        height: parent.height

        Row {
            topPadding: 10
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                text: "Add New Location"
                font.underline: true
                font.bold: true
            }
        }

        Row {
            topPadding: 15
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                horizontalAlignment: Text.AlignLeft
                text: "Name:"
                rightPadding: 20
            }

            TextField {
                id: locname
                placeholderText: qsTr("Living Room")
            }
        }

        Row {
            topPadding: 15
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                horizontalAlignment: Text.AlignLeft
                text: "IP Address:"
                rightPadding: 20
            }

            TextField {
                id: ipadd
                placeholderText: qsTr("192.168.X.X")
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                horizontalAlignment: Text.AlignLeft
                text: "Port:"
                rightPadding: 20
            }

            TextField {
                id: port
                placeholderText: qsTr("8080")
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                horizontalAlignment: Text.AlignLeft
                text: "Username:"
                rightPadding: 20
            }

            TextField {
                id: user
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter

            Label{
                horizontalAlignment: Text.AlignLeft
                text: "Password:"
                rightPadding: 20
            }

            TextField {
                id: pass
                echoMode: TextInput.Password
                passwordCharacter: "*"
            }
        }

        Row {
            topPadding: 15
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            Button {
                id: addtolist
                text: "Add Location"
                onClicked: {
                    userModel.append({"name": locname.text, "ip": ipadd.text, "iport": port.text, "uname": user.text, "pword": pass.text})
                    var datamodel = []
                    for (var i = 0; i < userModel.count; ++i) datamodel.push(userModel.get(i))
                    datastore = JSON.stringify(datamodel)
                }
            }

            Button {
                id: removefromlist
                text: "Remove Selected"
                onClicked: {
                    verifyDialog.open()
                }
            }
        }
        Row {
            topPadding: 15
            anchors.horizontalCenter: parent.horizontalCenter

            Rectangle {
                width: 275
                height: 150
                clip: true
                ListView {
                    id: list
                    anchors.fill: parent
                    model: userModel
                    delegate: Component {
                        Item {
                            width: list.width
                            height: 40
                            Column {
                                Text { text: name + "  " + ip }
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: list.currentIndex = index
                            }
                        }
                    }
                    highlight: Rectangle {
                        color: 'grey'
                        Text {
                            anchors.centerIn: parent
                            color: 'white'
                        }
                    }
                    focus: true
                }
            }
        }
    }
}

