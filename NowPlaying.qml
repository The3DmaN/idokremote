import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {

    id: pagenowplaying

    Item {
        anchors.centerIn: parent
        width: subsDialog.width
        height: subsDialog.height

        Dialog {
            id: subsDialog
            width: 300
            height: 120
            modal: true
            title: "Subtitles"

            ComboBox {
                id: subsCombo
                textRole: "lang"
                width: parent.width
                model: ListModel { id: subsModel }
                onActivated: {
                    var params =
                            "{\"playerid\":1,\"subtitle\":".concat(
                                subsModel.get(currentIndex).index, ", \"enable\":true}"
                                );
                    if (subsModel.get(currentIndex).index === "-1")
                    {
                        params = "{\"playerid\":1,\"subtitle\":\"off\"}";

                    }
                    request(
                                "POST",
                                "\"Player.SetSubtitle\",\"params\":".concat(params),
                                function (o) { processResults(o); }
                                );
                    subsDialog.close();
                }
            }
        }
    }

    Item {
        anchors.centerIn: parent
        width: audDialog.width
        height: audDialog.height

        Dialog {
            id: audDialog
            width: 300
            height: 120
            modal: true
            title: "Audio"

            ComboBox {
                id: audCombo
                textRole: "lang"
                width: parent.width
                model: ListModel { id: audModel }
                onActivated: {
                    var params =
                            "{\"playerid\":1,\"stream\":".concat(
                                audModel.get(currentIndex).index, "}"
                                );
                    request(
                                "POST",
                                "\"Player.SetAudioStream\",\"params\":".concat(params),
                                function (o) { processResults(o); }
                                );
                    audDialog.close();
                }
            }
        }
    }

    ColumnLayout {
        width: pagenowplaying.width
        Row {
            topPadding: 10
            Layout.alignment: Qt.AlignHCenter
            Label {
                id: titlelabel
                width: pagenowplaying.width - 20
                wrapMode: Text.WordWrap
                font.bold: true
                text: "No Content Playing"
                Item {
                    Timer {
                        interval: 500; running: true; repeat: true
                        onTriggered: {
                            request(
                                        "POST",
                                        "\"Player.GetItem\", \"params\": { \"properties\": [\"title\", \"album\", \"artist\", \"season\", \"episode\", \"duration\", \"showtitle\", \"tvshowid\", \"thumbnail\", \"file\", \"fanart\", \"streamdetails\"], \"playerid\": 1}",
                                        function (o)
                                        {
                                            var resp = processResults(o)["result"];
                                            for(var s in resp)
                                            {

                                                var titleresult = resp[s]["label"];
                                                if (!titleresult) {
                                                    titlelabel.text = "No Content Playing";
                                                } else {
                                                    titlelabel.text = titleresult;
                                                    mediaslider.start()
                                                }
                                            }

                                        }
                                        );
                        }
                    }
                }
                horizontalAlignment: Text.AlignHCenter
                color: "white"
            }
        }
        Row {
            topPadding: 10
            Layout.alignment: Qt.AlignHCenter
            Label {
                id: cplaytime
                topPadding: 15
                rightPadding: 10
                width: 70
                text: "00" + ":" + "00" + ":" + "00"
                horizontalAlignment: Text.AlignHCenter
                Item {
                    Timer {
                        interval: 500; running: true; repeat: true
                        onTriggered: {
                            request(
                                        "POST",
                                        "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"time\"]}",
                                        function (o)
                                        {
                                            var resp = processResults(o)["result"];
                                            for(var s in resp)
                                            {

                                                var mediahours = resp[s]["hours"];
                                                var fmediahours = ("0" + mediahours).slice(-2);
                                                var mediaminutes = resp[s]["minutes"];
                                                var fmediaminutes = ("0" + mediaminutes).slice(-2);
                                                var mediaseconds = resp[s]["seconds"];
                                                var fmediaseconds = ("0" + mediaseconds).slice(-2);

                                                cplaytime.text = fmediahours + ":" + fmediaminutes + ":" + fmediaseconds;
                                            }

                                        }
                                        );
                        }
                    }
                }
            }
            Slider {
                id: slidercont
                Timer {
                    id: mediaslider
                    interval: 500; running: false; repeat: true
                    onTriggered: {

                        if (!slidercont.pressed == true) {
                            request(
                                        "POST",
                                        "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"percentage\"]}",
                                        function (o)
                                        {
                                            var resp = processResults(o)["result"];
                                            for(var s in resp)
                                            {
                                                slidercont.value = resp[s];
                                            }

                                        }
                                        );
                        }
                    }
                }
                from: 0
                to: 100
                onMoved: {
                    request(
                                "POST",
                                "\"Player.Seek\",\"params\":{\"playerid\":1,\"value\":{\"percentage\":" + slidercont.visualPosition *100 + "}}",
                                function (o) { processResults(o); }
                                );
                }
            }
            Label {
                id: tplaytime
                topPadding: 15
                leftPadding: 10
                width: 70
                text: "00" + ":" + "00" + ":" + "00"
                horizontalAlignment: Text.AlignHCenter
                Item {
                    Timer {
                        interval: 500; running: true; repeat: true
                        onTriggered: {
                            request(
                                        "POST",
                                        "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"totaltime\"]}",
                                        function (o)
                                        {
                                            var resp = processResults(o)["result"];
                                            for(var s in resp)
                                            {

                                                var totalmediahours = resp[s]["hours"];
                                                var ftotalmediahours = ("0" + totalmediahours).slice(-2);
                                                var totalmediaminutes = resp[s]["minutes"];
                                                var ftotalmediaminutes = ("0" + totalmediaminutes).slice(-2);
                                                var totalmediaseconds = resp[s]["seconds"];
                                                var ftotalmediaseconds = ("0" + totalmediaseconds).slice(-2);

                                                tplaytime.text = ftotalmediahours + ":" + ftotalmediaminutes + ":" + ftotalmediaseconds;
                                            }

                                        }
                                        );
                        }
                    }
                }
            }
        }
        Row {
            topPadding: 1
            Layout.alignment: Qt.AlignHCenter
            spacing: 5

            Image {
                id: buttonskipleft
                width: 50
                height: 50
                source: "images/skipleft.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("skipleft clicked!")
                        request(
                                    "POST",
                                    "\"Player.GoTo\",\"params\":{\"playerid\":1,\"to\":\"previous\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonskipleft.scale = 1.10
                    }
                    onReleased: {
                        buttonskipleft.scale = 1
                    }
                }
            }

            Image {
                id: buttonffleft
                width: 50
                height: 50

                source: "images/ffleft.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("ffleft clicked!")
                        request(
                                    "POST",
                                    "\"Player.Seek\",\"params\":{\"playerid\":1,\"value\":\"smallbackward\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonffleft.scale = 1.10
                    }
                    onReleased: {
                        buttonffleft.scale = 1
                    }
                }
            }

            Image {
                id: buttonplaypause
                width: 50
                height: 50

                source: "images/playpause.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("play clicked!")
                        request(
                                    "POST",
                                    "\"Player.PlayPause\",\"params\":{\"playerid\":1}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonplaypause.scale = 1.10
                    }
                    onReleased: {
                        buttonplaypause.scale = 1
                    }
                }
            }

            Image {
                id: buttonstop
                width: 50
                height: 50

                source: "images/stop.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("stop clicked!")
                        request(
                                    "POST",
                                    "\"Player.Stop\",\"params\":{\"playerid\":1}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonstop.scale = 1.10
                    }
                    onReleased: {
                        buttonstop.scale = 1
                    }
                }
            }

            Image {
                id: buttonffright
                width: 50
                height: 50

                source: "images/ffright.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("ffright clicked!")
                        request(
                                    "POST",
                                    "\"Player.Seek\",\"params\":{\"playerid\":1,\"value\":\"smallforward\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonffright.scale = 1.10
                    }
                    onReleased: {
                        buttonffright.scale = 1
                    }
                }
            }

            Image {
                id: buttonskipright
                width: 50
                height: 50

                source: "images/skipright.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.info("skipright clicked!")
                        request(
                                    "POST",
                                    "\"Player.GoTo\",\"params\":{\"playerid\":1,\"to\":\"next\"}",
                                    function (o) { processResults(o); }
                                    );
                    }
                    onPressed: {
                        buttonskipright.scale = 1.10
                    }
                    onReleased: {
                        buttonskipright.scale = 1
                    }
                }
            }
        }
        Row {
            topPadding: 10
            Layout.alignment: Qt.AlignHCenter
            spacing: 5

            Image {
                id: buttonaudio
                width: 50
                height: 50
                source: "images/audio.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "GET",
                                    "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"currentaudiostream\",\"audiostreams\"]}",
                                    function (o)
                                    {
                                        var resp = processResults(o)["result"];

                                        var audio = resp["audiostreams"];
                                        audModel.clear();

                                        audCombo.currentIndex = 0;
                                        if (audio.length > 0)
                                        {
                                            for(var s in audio)
                                            {
                                                var lang = audio[s]["language"];
                                                audModel.append({
                                                                    "index": audio[s]["index"].toString(),
                                                                    "lang": "[".concat((lang.length > 0 ? lang : "unknown"), "] ", audio[s]["name"])
                                                                });
                                            }


                                            var currentAudio = resp["currentaudiostream"];
                                            if (currentAudio !== null)
                                            {
                                                for (var i = 0; i < audModel.count; i++)
                                                {
                                                    if (audModel.get(i).index === currentAudio["index"].toString())
                                                    {
                                                        audCombo.currentIndex = i;
                                                        break;
                                                    }
                                                }

                                            }
                                        }


                                        audDialog.open();
                                    }
                                    );
                    }
                    onPressed: {
                        buttonaudio.scale = 1.10
                    }
                    onReleased: {
                        buttonaudio.scale = 1
                    }
                }
            }
            Image {
                id: buttonsubs
                width: 50
                height: 50
                source: "images/subs.svg"
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        request(
                                    "GET",
                                    "\"Player.GetProperties\",\"params\":{\"playerid\":1,\"properties\":[\"subtitleenabled\",\"currentsubtitle\",\"subtitles\"]}",
                                    function (o)
                                    {
                                        var resp = processResults(o)["result"];

                                        var subtitles = resp["subtitles"];
                                        subsModel.clear();
                                        subsModel.append({
                                                             "index": "-1",
                                                             "lang": "- no subtitles -"
                                                         });
                                        subsCombo.currentIndex = 0;
                                        if (subtitles.length > 0)
                                        {
                                            for(var s in subtitles)
                                            {
                                                var lang = subtitles[s]["language"];
                                                subsModel.append({
                                                                     "index": subtitles[s]["index"].toString(),
                                                                     "lang": "[".concat((lang.length > 0 ? lang : "unknown"), "] ", subtitles[s]["name"])
                                                                 });
                                            }

                                            if(resp["subtitleenabled"] === true)
                                            {
                                                var currentSubtitle = resp["currentsubtitle"];
                                                if (currentSubtitle !== null)
                                                {
                                                    for (var i = 0; i < subsModel.count; i++)
                                                    {
                                                        if (subsModel.get(i).index === currentSubtitle["index"].toString())
                                                        {
                                                            subsCombo.currentIndex = i;
                                                            break;
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        subsDialog.open();
                                    }
                                    );

                    }
                    onPressed: {
                        buttonsubs.scale = 1.10
                    }
                    onReleased: {
                        buttonsubs.scale = 1
                    }
                }
            }
        }
    }

}
