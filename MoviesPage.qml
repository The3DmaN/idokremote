import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {

    id: pageMovies
    property string movname:  ""
    property string movid:  ""
    property string plot: ""
    property string cast: ""
    property string mpaa: ""
    property string search: ""
    Component.onCompleted: {
        request(
                    "POST",
                    "\"VideoLibrary.GetMovies\", \"id\": 1",
                    function (o)
                    {
                        var resp = processResults(o)["result"];

                        var movie = resp["movies"];
                        movModel.clear();

                        if (movie.length > 0)
                        {
                            for(var s in movie)
                            {
                                var name = movie[s]["label"];
                                var mid = movie[s]["movieid"];
                                movModel.append({
                                                    "name": movie[s]["label"],
                                                    "mid": movie[s]["movieid"]
                                                });
                            }

                        }
                    }
                    );


    }

    Dialog {
        id: movieDialog
        width: 300
        height: parent.height - 150
        anchors.centerIn: parent
        modal: true
        title: "Movie Info"
        onOpened: {
            request(
                        "POST",
                        "\"VideoLibrary.GetMovieDetails\",\"id\":1,\"params\": {\"movieid\": " + pageMovies.movid + ",\"properties\": [\"art\",\"cast\",\"dateadded\",\"director\",\"fanart\",\"file\",\"genre\",\"imdbnumber\",\"lastplayed\",\"mpaa\",\"originaltitle\",\"playcount\",\"plot\",\"plotoutline\", \"premiered\",\"rating\",\"runtime\",\"setid\",\"sorttitle\",\"streamdetails\",\"studio\",\"tagline\",\"thumbnail\",\"title\",\"trailer\",\"userrating\",\"votes\",\"writer\"]}",
                        function (o)
                        {
                            var resp = processResults(o)["result"];

                            var info = resp["moviedetails"];

                            //if (info.length > 0)
                            {
                                pageMovies.plot = info["plot"];
                                //    pageMovies.cast = info["cast"];
                                pageMovies.mpaa = info["mpaa"];
                            }
                        }
                        );
        }
        ScrollView {
            id: moviescrollinfo
            height: parent.height
            width: 255
            contentWidth: availableWidth
            clip: true
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            Column {
                anchors.fill: parent
                Row  {
                    Label {
                        width: moviescrollinfo.width
                        wrapMode: Text.WordWrap
                        horizontalAlignment : Text.AlignHCenter
                        text: pageMovies.movname
                        font.bold: true
                    }
                }
                ToolSeparator {
                    width: moviescrollinfo.width
                    orientation: Qt.Horizontal
                }
                Row {
                    spacing: 5
                    anchors.horizontalCenter: parent.horizontalCenter
                    Button {
                        text: "Play on Kodi"
                        onClicked: {
                            request(
                                        "POST",
                                        "\"Player.Open\",\"id\":1,\"params\":{\"item\":{\"movieid\":" + pageMovies.movid + "}}",
                                        function (o) { processResults(o); }
                                        );
                            movieDialog.close();
                            stackView.pop()
                            stackView.push("NowPlaying.qml")
                        }
                    }
                }
                ToolSeparator {
                    width: moviescrollinfo.width
                    orientation: Qt.Horizontal
                }
                Label {
                    text: "Movie Plot:"
                    font.bold: true
                }

                Row  {
                    TextArea {
                        width: moviescrollinfo.width
                        wrapMode: Text.WordWrap
                        readOnly: true
                        text: pageMovies.plot
                    }
                }
            }
        }
    }

    ScrollView {
        id: moviescroll
        anchors.fill: parent
        topPadding: 80
        clip: true
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff

        ListView {
            id: movList
            width: parent.width
            height: parent.height
            currentIndex: -1
            model: ListModel { id: movModel }
            highlight: Rectangle
                          {
                               color:"white"
                               radius: 5
                               opacity: 0.7
                               focus: true
                          }
            highlightFollowsCurrentItem: true
            highlightMoveDuration: 0
            focus: true
            delegate: Column {
                width: movList.width
                Text{
                    leftPadding: 10
                    topPadding: 10
                    bottomPadding: 10
                    color: "white"
                    font.pointSize: 13
                    text: name
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            movList.currentIndex = index
                            pageMovies.movname = movModel.get(movList.currentIndex).name
                            pageMovies.movid = movModel.get(movList.currentIndex).mid
                            movieDialog.open()

                        }
                    }
                }
            }
        }
    }
    Row {
        id: searchRow
        width: moviescroll.width
        leftPadding: 10
        rightPadding: 10
        topPadding: 10
        Rectangle {
            id: searchBox
            width: moviescroll.width - 20
            height: 40
            color: "white"
            Row {
                spacing: 10
                TextInput {
                    id: mediaSearch
                    topPadding: 10
                    leftPadding: 10
                    width: moviescroll.width - 70
                    color: "Black"
                    font.pointSize : 12
                    clip: true
                    onTextEdited: {
                        request(
                                    "POST",
                                    "\"VideoLibrary.GetMovies\", \"id\": \"1\", \"params\": {\"filter\": {\"operator\": \"contains\", \"field\": \"title\", \"value\": \""+ mediaSearch.text +"\"}, \"properties\": [\"title\", \"art\", \"file\"]}",
                                    function (o)
                                    {
                                        var resp = processResults(o)["result"];

                                        var movie = resp["movies"];
                                        movModel.clear();

                                        if (movie.length > 0)
                                        {
                                            for(var s in movie)
                                            {
                                                var name = movie[s]["label"];
                                                var mid = movie[s]["movieid"];
                                                movModel.append({
                                                                    "name": movie[s]["label"],
                                                                    "mid": movie[s]["movieid"]
                                                                });
                                            }

                                        }
                                    }
                                    );
                    }
                }
                Row {
                    topPadding: 4
                    Image {
                        height: 32
                        width: 35
                        source: "images/search.svg"
                    }
                }
            }
        }
    }
}
